package com.delaroystudios.sqlitelogin.activities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by edna1 on 22/02/2018.
 */

public class HelperBD extends SQLiteOpenHelper {
    public HelperBD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table operaciones(suma integer);");

    }
    public void insertar (String suma){
    ContentValues valores = new ContentValues();
        valores.put("suma",suma);

        this.getWritableDatabase().insert("operaciones", null, valores);
}

    public String leerTodos(){
        String consulta = "";
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM operaciones", null);
        if (cursor.moveToFirst()){
            do{
                String Psuma = cursor.getString(cursor.getColumnIndex("suma"));


                consulta += Psuma  + "\n";
            }while (cursor.moveToNext());
        }
        return consulta;


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
