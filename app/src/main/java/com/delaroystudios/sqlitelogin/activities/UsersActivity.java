package com.delaroystudios.sqlitelogin.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.delaroystudios.sqlitelogin.R;

/**
 * Created by delaroy on 3/27/17.
 */
public class UsersActivity extends AppCompatActivity {

    EditText numero1, numero2, datos;
    Button sumar, restar, multipli, divi, calco, listado;
    TextView lista;
    HelperBD estudiantedb;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        estudiantedb = new HelperBD(this, "bda", null, 1);
        numero1=(EditText)findViewById(R.id.txtprimernumero);
        numero2=(EditText)findViewById(R.id.txtsegundonumero);
        sumar=(Button)findViewById(R.id.btnSuma);
        restar=(Button)findViewById(R.id.btnResta);
        multipli=(Button)findViewById(R.id.btnMultiplicar);
        divi=(Button)findViewById(R.id.btnDividir);
        calco=(Button)findViewById(R.id.btnCalcular);
        listado=(Button)findViewById(R.id.btnlista);
        datos=(EditText) findViewById(R.id.lblDatos);
        lista=(TextView)findViewById(R.id.lblListar);
        sumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int valor1= Integer.parseInt(numero1.getText().toString());
                int valor2= Integer.parseInt(numero2.getText().toString());
                Integer X=valor1+valor2;
                datos.setText("La suma es "+X);
                estudiantedb.insertar(datos.getText().toString());
            }
        });
        restar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int valor1= Integer.parseInt(numero1.getText().toString());
                int valor2= Integer.parseInt(numero2.getText().toString());
                Integer X=valor1-valor2;
                datos.setText("La resta es "+X);
                estudiantedb.insertar(datos.getText().toString());
            }
        });
        multipli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int valor1= Integer.parseInt(numero1.getText().toString());
                int valor2= Integer.parseInt(numero2.getText().toString());
                Integer X=valor1*valor2;
                datos.setText("La multiplicacion  es "+X);
                estudiantedb.insertar(datos.getText().toString());
            }
        });
        divi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int valor1= Integer.parseInt(numero1.getText().toString());
                int valor2= Integer.parseInt(numero2.getText().toString());
                Integer X=valor1/valor2;
                datos.setText("La division  es "+X);
                estudiantedb.insertar(datos.getText().toString());
            }
        });
        listado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lista.setText("");
                lista.append(estudiantedb.leerTodos());
            }
        });
        calco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
